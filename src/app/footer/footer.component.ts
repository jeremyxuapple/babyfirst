import { Component, OnInit } from '@angular/core';
import { OffersService } from '../offers.service';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  images:any;
  constructor(
    private offersService: OffersService
  ) { }

  ngOnInit() {
    this.images = this.offersService.getImages();
  }

}
