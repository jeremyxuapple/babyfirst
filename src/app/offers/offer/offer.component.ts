import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {

  @Input() product: any;
  @Input() images: any;
  @Output() checkOffer: EventEmitter<any> = new EventEmitter();
  showModal = false;

  constructor(
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

  modalToggle() {
    this.showModal = !this.showModal;
  }

  getStyle() {
    if (this.product.checked) {
      var style = this.sanitizer.bypassSecurityTrustStyle("border: 3px solid #58D68D");
    } else {
      var style = this.sanitizer.bypassSecurityTrustStyle("border: 3px solid #2b2432");
    }
    return style;
  }

  check() {

    this.product.checked = !this.product.checked;
    const data = {
      product_id: this.product.id,
      checked: this.product.checked
    }
    this.checkOffer.emit(data);
  }

  detailCheck() {
    this.check();
    this.showModal = false;
  }
}
