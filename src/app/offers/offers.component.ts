import { Component, OnInit } from '@angular/core';

import { OffersService } from '../offers.service';

@Component({
  selector: 'offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {
  hideModal = false;
  products = [];
  images = {};
  offerNumberChecked = 0;
  offerNumber = '0 of 5 offers selected';

  constructor(
    private offerService: OffersService,
  ) { }

  ngOnInit() {
    this.products = this.offerService.getProducts();
    this.images = this.offerService.getImages();

    this.products.forEach((product) => {
      product.checked = false;
      product.tagClass = product.tag.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "");;
    });

  }

  offerNumberUpdate(data) {

    if (data.checked && this.offerNumberChecked <= 5) {
      this.offerNumberChecked  += 1;
    } else if (!data.checked && this.offerNumberChecked <= 5 && this.offerNumberChecked >= 0) {
      this.offerNumberChecked  -= 1;
    }

    // handle when the offerNumberChecked is already 5
    if (this.offerNumberChecked == 6) {
      this.offerNumberChecked -= 1;
      this.products.forEach((product) => {
        if (product['id'] == data.product_id) {
          product.checked = !data.checked;
        }
      });

    }
    this.offerNumber = this.offerNumberChecked + ' of 5 offers selected';
  }

  submitRequest() {
    window.location.replace("https://www.sampler.io");
  }

}
