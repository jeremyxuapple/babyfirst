import { Injectable } from '@angular/core';

@Injectable()
export class OffersService {

  constructor() { }

  getImages() {
    return {
      'tooltipIcon': 'https://upload.wikimedia.org/wikipedia/commons/e/e4/Infobox_info_icon.svg',
      'icon_checkPNG': 'https://hermanxu89.000webhostapp.com/images/icon_check.svg',
      'icon_checkSVG':'https://hermanxu89.000webhostapp.com/images/icon_check.svg',
      'img_braceletJPG':'https://hermanxu89.000webhostapp.com/images/img_bracelet.jpg',
      'img_gelJPG': 'https://hermanxu89.000webhostapp.com/images/img_gel.jpg',
      'img_gogglesJPG': 'https://hermanxu89.000webhostapp.com/images/img_goggles.jpg',
      'img_logoPNG': 'https://hermanxu89.000webhostapp.com/images/img_logo.png',
      'img_lotionJPG': 'https://hermanxu89.000webhostapp.com/images/img_lotion.jpg',
      'img_padsJPG': 'https://hermanxu89.000webhostapp.com/images/img_pads.jpg',
      'img_powderJPG': 'https://hermanxu89.000webhostapp.com/images/img_powder.jpg',
      'img_publisher_logoPNG': 'https://hermanxu89.000webhostapp.com/images/img_publisher_logo.png',
      'img_publisher_logoSVG': 'https://hermanxu89.000webhostapp.com/images/img_publisher_logo.svg',
      'img_smoothieJPG': 'https://hermanxu89.000webhostapp.com/images/img_smoothie.jpg',
      'img_sprayJPG': 'https://hermanxu89.000webhostapp.com/images/img_spray.jpg',
      'img_toothpasteJPG': 'https://hermanxu89.000webhostapp.com/images/img_toothpaste.jpg',
      'img_wipesJPG': 'https://hermanxu89.000webhostapp.com/images/img_wipes.jpg'
    }

  }

  getProducts() {
    const images = this.getImages()
    return [
          {
              "id": 1,
              "name": "Toothpaste Strawberry Mint",
              "description": "Our Kids Toothpaste delivers delightful, fruity freshness that kids love! Our fluoride-free formula helps brighten teeth, promote healthy teeth and gums, and fight tartar buildup.",
              "image": images.img_toothpasteJPG,
              "tag": "All Ages",
              "brand": "The Honest Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 6,
              "name": "Baby Lotion",
              "description": "Then we'll go with that data file! Can we have Bender Burgers again? Soothe us with sweet lies. What's with you kids? Every other day it's food, food, food. Alright, I'll get you some stupid food.",
              "image": images.img_lotionJPG,
              "tag": "Toddler",
              "brand": "The Fry Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 12,
              "name": "Lavender Hand Sanitizer Gel",
              "description": "We'll go deliver this crate like professionals, and then we'll go home. I've got to find a way to escape the horrible ravages of youth. ",
              "image": images.img_gelJPG,
              "tag": "Kids 4+",
              "brand": "The Bender Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 23,
              "name": "Pear Kiwi & Apple Smoothie",
              "description": "I videotape every customer that comes in here, so that I may blackmail them later. Who are you, my warranty?! So I really am important?",
              "image": images.img_smoothieJPG,
              "tag": "New Born",
              "brand": "The Leela Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 49,
              "name": "Disposable Nursing Bra Pads",
              "description": "Are you crazy? I can't swallow that. Have you ever tried just turning off the TV, sitting down with your children, and hitting them? Belligerent and numerous. Hey, whatcha watching?",
              "image": images.img_padsJPG,
              "tag": "All Ages",
              "brand": "The Zoidberg Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 66,
              "name": "Baby Powder",
              "description": "Our love isn't any different from yours, except it's hotter, because I'm involved. Please, Don-Bot… look into your hard drive, and open your mercy file!",
              "image": images.img_powderJPG,
              "tag": "Toddler",
              "brand": "The Amy Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 71,
              "name": "Pacifier Bracelet",
              "description": "Oh, how awful. Did he at least die painlessly? …To shreds, you say. Well, how is his wife holding up? …To shreds, you say. ",
              "image": images.img_braceletJPG,
              "tag": "Kids 4+",
              "brand": "The Professor Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 80,
              "name": "Diaper Ointment",
              "description": "I love you, buddy! I could if you hadn't turned on the light and shut off my stereo. Too much work. Let's burn it and say we dumped it in the sewer.",
              "image": images.img_sprayJPG,
              "tag": "Newborn",
              "brand": "The Hermes Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 164,
              "name": "Baby Googles",
              "description": "It must be wonderful. What are their names? Oh sure! Blame the wizards! Man, I'm sore all over. I feel like I just went ten rounds with mighty Thor",
              "image": images.img_gogglesJPG,
              "tag": "Toddler",
              "brand": "The Hubert Company",
              "logo" : images.img_logoPNG
          },
          {
              "id": 734,
              "name": "Aloe Vera Baby Wipes",
              "description": "Bender, you risked your life to save me! And remember, don't do anything that affects anything, unless it turns out you were supposed to, in which case, for the love of God, don't not do it!",
              "image": images.img_wipesJPG,
              "tag": "Kids 4+",
              "brand": "The Nibbler Company",
              "logo" : images.img_logoPNG
          }
      ];
  }

}
